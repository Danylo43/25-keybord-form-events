"use stric";

//- У файлі index.html лежить розмітка для кнопок.
//- Кожна кнопка містить назву клавіші на клавіатурі
//- Після натискання вказаних клавіш - та кнопка,
//на якій написана ця літера, повинна фарбуватися
//в синій колір. При цьому якщо якась інша літера
//вже раніше була пофарбована в синій колір - вона
//стає чорною. Наприклад за натисканням Enter перша
//кнопка забарвлюється у синій колір. Далі, користувач натискає S,
//і кнопка S забарвлюється в синій колір, а кнопка
//Enter знову стає чорною.
//Але якщо при натискані на кнопку її не існує в розмітці,
//то попередня активна кнопка повина стати неактивною.

const buttons = document.querySelectorAll(".key");

function removeClass() {
  buttons.forEach((btn) => btn.classList.remove("active"));
}

document.addEventListener("keydown", (e) => {
  removeClass();

  buttons.forEach((btn) => {
    if (btn.textContent.toLowerCase() === e.key.toLowerCase())
      btn.classList.add("active");
  });
});

document.addEventListener("keyup", (e) => {
  removeClass();

  buttons.forEach((btn) => {
    if (btn.textContent.toLowerCase() === e.key.toLowerCase())
      btn.classList.remove("active");
  });
});
